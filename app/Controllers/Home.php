<?php

namespace App\Controllers;
use App\Models\Product;
class Home extends BaseController
{
	public function index()
	{
	

		$db = \Config\Database::connect();

		$query_products = $db->query('SELECT * FROM products');
		$productos = $query_products->getResultArray();

		$query = $db->query('SELECT * FROM companies');
		$empresa = $query->getFirstRow();
		$session = session();
		$nombre_empresa=$empresa->name;
		$data = [
			"empresa_name" => $nombre_empresa,
		];
		$session->set($data);
		$query_category = $db->query('SELECT * FROM categories');
		$categorias = $query_category->getResultArray();
		return view('Front/home',['empresa' =>$empresa,'categorias' =>$categorias,'productos' =>$productos]);
	}
}
