<?php

namespace App\Controllers;
use App\Models\Company;
class Empresa extends BaseController
{
	public function index()
	{
		$db = \Config\Database::connect();
		$query = $db->query('SELECT * FROM companies');
		$empresa = $query->getFirstRow();
		$query_user = $db->query('SELECT * FROM users');
		$user = $query_user->getFirstRow();
		return view('Admin/empresa/index',['empresa' =>$empresa,'user' =>$user]);
	}

	public function edit($id = 0)
	{
		$model = new Company($db);
		$empresa = $model->find($id);
		return view('Admin/empresa/edit',['empresa'=>$empresa]);
		
	}

	public function update()
	{
		$model = new Company();
	
		$imageName =  'logo.jpeg';

		if(!empty($this->request->getPost('Imagecroped'))){
        
		//guardando imagen
		$imageString=$this->request->getPost('Imagecroped');
		$filepath = "./uploads/admin/".$imageName;
		$imageUpload = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imageString));
		file_put_contents($filepath,$imageUpload);
        }

		$data = [
			'id' => $this->request->getPost('id'),
			'name' => $this->request->getPost('nombre'),
			'address' => $this->request->getPost('direccion'),
			'about' => $this->request->getPost('descripcion'),
			'provincia' => $this->request->getPost('provincia'),
			'canton' => $this->request->getPost('canton'),
			'teluno' => $this->request->getPost('teluno'),
			'teldos' => $this->request->getPost('teldos'),
			'image' => $imageName,
		];
		$model->save($data);
		return redirect()->to(base_url('/admin/home'));
		

	}	
	
}