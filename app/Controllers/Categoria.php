<?php

namespace App\Controllers;
use App\Models\Category;
class Categoria extends BaseController
{
	public function index()
	{
		$db = \Config\Database::connect();
		$query = $db->query('SELECT * FROM categories WHERE categories.id NOT IN (SELECT DISTINCT products.category_id FROM products WHERE categories.id = products.category_id)');
		$categorias = $query->getResultArray();
		$query_con = $db->query('SELECT DISTINCT categories.id, categories.name FROM categories INNER JOIN products ON categories.id=products.category_id ORDER by categories.id ASC');
		$con_categorias = $query_con->getResultArray();
		return view('Admin/categoria/index',['categorias' =>$categorias,'con_categorias' =>$con_categorias]);
	}
	public function create()
	{
		$validation=\Config\Services::validation();
		return view('Admin/categoria/create',['validation'=>$validation]);
	}
	public function store()
	{

		if($this->validate('category')){

			$model = new Category();
			$data = [
				'name' => $this->request->getPost('nombre')
			];
			$model->save($data);
			return redirect()->to(base_url('/admin/categoria'));
		}
		return redirect()->back()->withInput();
	}
	public function edit($id = 0)
	{
		$model = new Category($db);
		$categoria = $model->find($id);
		$validation=\Config\Services::validation();
		return view('Admin/categoria/edit',['validation'=>$validation,'categoria'=>$categoria]);
	}
	public function delete($id = 0)
	{
		$model = new Category($db);
		if($model->find($id)){
			$model->delete($id);
	  	}
		return redirect()->to(base_url('/admin/categoria'));
	}

	public function update()
	{

		if($this->validate('category')){

			$model = new Category();
			$data = [
				'id' => $this->request->getPost('id'),
				'name' => $this->request->getPost('nombre'),
			];
			$model->save($data);
			return redirect()->to(base_url('/admin/categoria'));
		}
		return redirect()->back()->withInput();
	}

	public function show($id = 0)
	{
		$db = \Config\Database::connect();
		
		$query_products = $db->query('SELECT * FROM products where products.category_id='.$id);
		$productos = $query_products->getResultArray();

		$model = new Category($db);
		$categoria = $model->find($id);

		return view('Admin/categoria/show',['categoria' =>$categoria,'productos' =>$productos]);
	}

	
}