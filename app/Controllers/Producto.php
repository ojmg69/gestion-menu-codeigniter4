<?php

namespace App\Controllers;
use App\Models\Category;
use App\Models\Product;
class Producto extends BaseController
{
	public function index()
	{
		$pager = \Config\Services::pager();
		$db = \Config\Database::connect();
		$data = new Product();
		$data->obtenerProductos();
		$productos=$data->paginate(5);
		$paginador=$data->pager;
		$query = $db->query('SELECT * FROM categories');
		$categorias = $query->getResultArray();
		$paginador->setPath('admin/producto');
		return view('Admin/productos/index',['productos' =>$productos,'paginador' =>$paginador,'categorias' =>$categorias]);
	}
	public function create()
	{
		$db = \Config\Database::connect();
		$validation=\Config\Services::validation();
		$query = $db->query('SELECT * FROM categories');
		$categorias = $query->getResultArray();
		return view('Admin/productos/create',['validation'=>$validation,'categorias'=>$categorias]);
	}
	public function store()
	{
		$db = \Config\Database::connect();

		if($this->validate('product')){
			var_dump($this->request);
			$model = new Product();
			$data = [
				'name' => $this->request->getPost('nombre'),
				'description' => $this->request->getPost('descripcion'),
				'options' => $this->request->getPost('options'),
				'extras' => $this->request->getPost('extras'),
				'price' => $this->request->getPost('precio'),
				'active' => $this->request->getPost('active'),
				'category_id' => $this->request->getPost('category_id'),
			];
			$model->save($data);
			$product_id = $model->getInsertID();

			$imageName = !empty($this->request->getPost('Imagecroped')) ?'image'.$product_id.'.jpeg':'default.jpg';
			if(!empty($this->request->getPost('Imagecroped'))){
			
			//guardando imagen
			$imageString=$this->request->getPost('Imagecroped');
			$filepath = "./uploads/productos/".$imageName;
			$imageUpload = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imageString));
			file_put_contents($filepath,$imageUpload);
			}
			$data = [
				'id'=>$product_id,
				'image' => $imageName,
			];
			$model->save($data);
		
			return redirect()->to(base_url('/admin/producto'));
		}
		return redirect()->back()->withInput();
	}
	public function edit($id = 0)
	{
		$db = \Config\Database::connect();
		$query = $db->query('SELECT id, name FROM categories');
		$categorias = $query->getResultArray();

		$return = array();
		if($query->getNumRows() > 0) {
		  foreach($query->getResultArray() as $row) {
			$return[$row['id']] = $row['name'];
		  }
		}
		
		$validation=\Config\Services::validation();
		$model_prod = new Product($db);
		$producto = $model_prod->find($id);
		return view('Admin/productos/edit',['validation'=>$validation,'categorias'=>$return,'producto'=>$producto]);
	}
	public function show($id = 0)
	{
		$model = new Product($db);
		$producto = $model->find($id);
		return view('Admin/productos/show',['producto'=>$producto]);
	}
	public function delete($id = 0)
	{
		$model = new Product($db);
		if($model->find($id)){
			$model->delete($id);
	  	}
		return redirect()->to(base_url('/admin/producto'));
	}

	public function update()
	{
		$db = \Config\Database::connect();

		//if($this->validate('product')){
			var_dump($this->request);
			$model = new Product();
			$data = [
				'id' => $this->request->getPost('id'),
				'name' => $this->request->getPost('nombre'),
				'description' => $this->request->getPost('descripcion'),
				'options' => $this->request->getPost('opciones'),
				'extras' => $this->request->getPost('extras'),
				'price' => $this->request->getPost('precio'),
				'active' => $this->request->getPost('active'),
				'category_id' => $this->request->getPost('category_id'),
			];
			$model->save($data);
			$product_id = $this->request->getPost('id');

			$imageName = 'image'.$product_id.'.jpeg';
			if(!empty($this->request->getPost('Imagecroped'))){
			
			//guardando imagen
			$imageString=$this->request->getPost('Imagecroped');
			$filepath = "./uploads/productos/".$imageName;
			$imageUpload = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imageString));
			file_put_contents($filepath,$imageUpload);
			$data = [
				'id'=>$product_id,
				'image' => $imageName,
			];
			$model->save($data);
			}
			return redirect()->to(base_url('/admin/producto'));
		/*}
		return redirect()->back()->withInput();*/
	}

	
}