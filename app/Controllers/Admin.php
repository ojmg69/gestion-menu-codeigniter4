<?php

namespace App\Controllers;
use App\Models\User;
use App\Models\Category;
class Admin extends BaseController
{
	public function index()
	{
		$db = \Config\Database::connect();
		$query_category = $db->query('SELECT count(*) as total FROM categories');
		$total_categorias = $query_category->getFirstRow();
		$query_productos = $db->query('SELECT count(*) as total FROM products');
		$total_productos = $query_productos->getFirstRow();
		return view('Admin/dashboard',['total_categorias' => $total_categorias,'total_productos' => $total_productos]);
	}

	public function login()
	{
		$session = session();
		if(empty($session->get('user'))){
			$mensaje = session('mensaje');
			return view('Admin/login');
		}else{
			return redirect()->to(base_url('/admin/home'))->with('mensaje','1');
		}
		
		
	}
	public function register()
	{
		return view('Admin/registrar');
	}

	public function loginpost()
	{
		$email = $this->request->getPost('email');
		$password = $this->request->getPost('password');
		$Usuario = new User();

		$datosUsuario = $Usuario->obtenerUsuario(['email' => $email]);

		if (count($datosUsuario) > 0 && 
			password_verify($password, $datosUsuario[0]['password'])) {

			$data = [
						"user" => $datosUsuario[0]['user'],
						"type" => $datosUsuario[0]['type']
					];

			$session = session();
			$session->set($data);

			return redirect()->to(base_url('/admin/home'))->with('mensaje','1');

		} else {
			return redirect()->to(base_url('/'))->with('mensaje','0');
		}
	}

	public function logout() {
		$session = session();
		$session->destroy();
		return redirect()->to(base_url('/'));
	}

	public function edit()
	{
		$db = \Config\Database::connect();
		$query_user = $db->query('SELECT * FROM users');
		$user = $query_user->getFirstRow();
		return view('Admin/editpass', ["user" => $user]);
		
	}
	public function update()
	{
		$model = new User();
		if(empty($this->request->getPost('password'))){
			$data = [
				'id' => $this->request->getPost('id'),
				'email' => $this->request->getPost('email'),
			];
		} else {
			$data = [
				'id' => $this->request->getPost('id'),
				'email' => $this->request->getPost('email'),
				'password' => password_hash($this->request->getPost('password'),PASSWORD_DEFAULT)
			];
		}
			$model->save($data);
			$this->logout();
			return redirect()->to(base_url('/'));
	}

}