<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use \CodeIgniter\I18n\Time;
class Category extends Seeder
{
	public function run()
	{

		$data = [
				[
				'name' => 'Arroces',
				'created_at'  => Time::now(),
				'updated_at'  => Time::now()
				],
				[
				'name' => 'Hamburguesas',
				'created_at'  => Time::now(),
				'updated_at'  => Time::now()
				],
				[
				'name' => 'Pizzas',
				'created_at'  => Time::now(),
				'updated_at'  => Time::now()
				]
                        
                ];

                // Using Query Builder
        //$this->db->table('categories')->insert($data);
		$this->db->table('categories')->insertBatch($data);
	}
}