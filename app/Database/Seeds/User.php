<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class User extends Seeder
{
	public function run()
	{
		$usuario = "admin";
		$email = "admin@admin.com";
		$password = password_hash("123", PASSWORD_DEFAULT);
		$type = "admin";

		$data = [
                        'user' => $usuario,
                        'email' => $email,
                        'password' => $password,
                        'type' => $type
                ];

                // Using Query Builder
        $this->db->table('users')->insert($data);
	}
}
