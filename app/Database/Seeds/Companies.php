<?php

namespace App\Database\Seeds;
use \CodeIgniter\I18n\Time;
use CodeIgniter\Database\Seeder;

class Companies extends Seeder
{
	public function run()
	{
		$data = [

			[
			'name' => 'Nombre Empresa',
            'address' => 'Direccion de la empresa',
            'about' => 'Texto corto acerca de la empresa',
            'provincia' => 'Provincia',
            'canton' => 'Canton',
            'teluno' => '00000000',
            'teldos' => '00000000',
            'image' => 'logo.jpeg',
			'created_at'  => Time::now(),
			'updated_at'  => Time::now()
			]
					
			];

	
	$this->db->table('companies')->insertBatch($data);
	}
}
