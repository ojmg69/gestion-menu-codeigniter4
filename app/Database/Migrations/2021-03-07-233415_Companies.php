<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Companies extends Migration
{
	
	public function up()
	{
			$this->forge->addField([
					'id'          => [
							'type'           => 'INT',
							'constraint'     => 5,
							'unsigned'       => true,
							'auto_increment' => true,
					],

					'name'       => [
							'type'           => 'VARCHAR',
							'constraint'     => '255',
					],

					'address'       => [
							'type'           => 'VARCHAR',
							'constraint'     => '255',
					],

					'about'       => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
					],

					'provincia'       => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
					],

					'canton'       => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
					],

					'teluno'       => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
					],

					'teldos'       => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
					],	
					
					'image'       => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
					],	


					"`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP",
					"`updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP",
					"`deleted_at` datetime NULL",

			]);
			$this->forge->addKey('id', true);
			$this->forge->createTable('companies');
	}

	public function down()
	{
			$this->forge->dropTable('companies');
	}
}
