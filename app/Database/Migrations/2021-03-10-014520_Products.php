<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;


class Products extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();
			$this->forge->addField([
					'id'				=> [
							'type'           => 'INT',
							'constraint'     => 5,
							'unsigned'       => true,
							'auto_increment' => true,
					],

					'name'      		=> [
							'type'           => 'VARCHAR',
							'constraint'     => '255',
					],

					'description'       => [
							'type'          => 'TEXT',
							'constraint'    => '255',
					],

					'options'      		=> [
							'type'          => 'VARCHAR',
							'constraint'    => '255',
							'null' 			=> true,
					],

					'extras'       		=> [
							'type'     		=> 'VARCHAR',
							'constraint'    => '255',
							'null' 			=> true,

					],

					'price'       		=> [
							'type' 			=> 'DECIMAL',
							'constraint' 	=> '10,2',
					],

					'image'				=> [
							'type'          => 'VARCHAR',
							'constraint'    => '255',
							'null' 			=> true,
					],
					'active'       		=> [
							'type'			=> 'BOOLEAN',
							'default'   	=> 1,
						
					],

					'category_id'       => [
						'type'           => 'INT',
						'constraint'     => 5,
						'null'      	 => true,
					],	
					"`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
					"`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",

			]);
			$this->forge->addPrimaryKey('id');
			$this->forge->addKey('category_id');
			$this->forge->addForeignKey('category_id','categories','id','NULL','CASCADE');
			$this->forge->createTable('products');
			
			$this->db->enableForeignKeyChecks();

	}

	public function down()
	{
			$this->forge->dropTable('products');
	}
}