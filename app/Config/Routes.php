<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

//admin

$routes->group('admin',['filter'=>'SesionAdmin'], function ($routes) {
	$routes->get('/', 'Admin::index');
	$routes->get('home', 'Admin::index');
	$routes->get('register', 'Admin::register');
	$routes->get('update', 'Admin::edit');
	$routes->post('update', 'Admin::update');

	//categorias
	$routes->get('categoria', 'Categoria::index');
	
	$routes->get('categoria/create', 'Categoria::create');
	$routes->post('categoria/store', 'Categoria::store');

	$routes->get('categoria/edit/(:num)', 'Categoria::edit/$1');
	$routes->post('categoria/update', 'Categoria::update');

	$routes->get('categoria/delete/(:num)', 'Categoria::delete/$1');

	//productos
	$routes->get('producto', 'Producto::index');
	$routes->get('producto/create', 'Producto::create');
	$routes->post('producto/store', 'Producto::store');
	$routes->get('producto/edit/(:num)', 'Producto::edit/$1');
	$routes->get('producto/show/(:num)', 'Producto::show/$1');
	$routes->post('producto/update', 'Producto::update');
	$routes->get('producto/delete/(:num)', 'Producto::delete/$1');

	//empresa
	$routes->get('empresa', 'Empresa::index');

	$routes->get('empresa/edit/(:num)', 'Empresa::edit/$1');
	$routes->post('empresa/update', 'Empresa::update');
	
});
//login
$routes->get('/login', 'Admin::login');
$routes->post('/login', 'Admin::loginpost');
$routes->get('/logout', 'Admin::logout');

//public
$routes->get('categoria/show/(:num)', 'Categoria::show/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}