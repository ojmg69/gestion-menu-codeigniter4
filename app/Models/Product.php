<?php

namespace App\Models;

use CodeIgniter\Model;

class Product extends Model
{
    protected $table      = 'products';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['name', 'description', 'options', 'extras', 'price', 'image', 'active', 'category_id'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function obtenerProductos() {
        $db      = \Config\Database::connect();
        $builder = $db->table('products');
        $builder->join('categories', 'categories.id = products.category_id', 'LEFT');
        $builder->select('products.id, products.name, products.active, products.description, products.price, categories.name AS `category_name`',FALSE);
        $builder->orderBy("products.id DESC");
		$query = $builder->get();
        return $query;
    }
}