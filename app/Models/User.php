<?php namespace App\Models;
	use CodeIgniter\Model;

	class User extends Model {

		protected $table      = 'users';
		protected $primaryKey = 'id';
		protected $returnType     = 'array';
		protected $useSoftDeletes = false;
	
		protected $allowedFields = [ 'email', 'password'];
	
		protected $useTimestamps = false;
		protected $createdField  = 'created_at';
		protected $updatedField  = 'updated_at';
		protected $validationRules    = [];
		protected $validationMessages = [];
		protected $skipValidation     = false;
		
		public function obtenerUsuario($data) {
			$Usuario = $this->db->table('users');
			$Usuario->where($data);
			return $Usuario->get()->getResultArray();
		}
	}