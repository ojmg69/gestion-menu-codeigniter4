<?php

namespace App\Models;

use CodeIgniter\Model;

class Company extends Model
{
    protected $table      = 'companies';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [ 'name', 'address', 'about', 'provincia', 'canton', 'teluno', 'teldos', 'image'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}