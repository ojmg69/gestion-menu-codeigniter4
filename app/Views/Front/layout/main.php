<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('public/css/styles_admin.css')?>">
    <title><?= $this->renderSection('title')?></title>
</head>

<body>
    <?= $this->include('Front/layout/navigation')?>
    <?= $this->renderSection('content')?>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <div class="container text-center">

        <!-- .........BLOQUE 4 perfil para footer.............. -->
        <footer class="blog-footer">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <img class="logo-foot rounded-circle z-depth-2" alt="100x100"
                        src="<?= base_url('uploads/admin/logo.jpeg')?>" data-holder-rendered="false">
                </div>
                <div class="col-md-4 col-lg-4">
                    <p><?=
                    		
                            session()->get('empresa_name') ?></p>
                </div>
                <div class="col-md-4 col-lg-4">
                    <p>
                        <a href="#">Ir arriba</a>
                    </p>
                </div>
            </div>

        </footer>
    </div>
</body>

</html>