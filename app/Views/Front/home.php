<?= $this->extend('Front/layout/main')?>
<?= $this->section('content')?>
<div class="container-fluid">
    <!-- ..........BLOQUE 1 CATEGORIAS........... -->
    <!-- Categorias asociadas a productos como links hacia los id, para navegar en categorias de productos Con esto yo hare un menu de navegacion -->
    <!-- Ej: -->
    <div class="row">
        <ul>
            <?php foreach ($categorias as $categoria): ?>
            <li>
                <a href="<?= base_url('categoria/show/'.$categoria['id'])?>"><?= $categoria['name'] ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <!-- .........BLOQUE 2 PERFIL DE EMPRESA.............. -->
    <!-- Despues del menu de navegacion aqui voy a colocar el perfil de la empresa con todos las datos que estan en la tabla -->
    <!-- Mostrar los datos sin css, limpios -->
    <div class="row">
        <div class="col-md-4">
            <div class="text-center">
                <img class="img-thumbnail" style="max-width: 250px;
                 height: auto;" src="<?= base_url('uploads/admin/'.$empresa->image)?>" alt="Empresa">
            </div>
            <div class="col text-center">
                <h3 class="font-weight-bold"><?= $empresa->name ?></h3>
            </div>
            <br />
            <strong><i class="fas fa-book mr-1"></i> Descripción</strong>

            <p class="text-muted">
                <?= $empresa->about ?>
            </p>
            <hr>
            <strong><i class="fas fa-map-marker-alt mr-1"></i> Dirección</strong>
            <p class="text-muted"><?= $empresa->address ?></p>
            <hr>
            <strong><i class="fas fa-map-marker-alt mr-1"></i> Provincia</strong>
            <p class="text-muted"> <?= $empresa->provincia ?></p>
            <hr>
            <strong><i class="fas fa-map-marker-alt mr-1"></i> Cantón</strong>
            <p class="text-muted"><?= $empresa->canton ?></p>
            <hr>
            <strong><i class="fas fa-phone-alt mr-1"></i> Telefonos</strong>

            <p class="text-muted"> <?= $empresa->teluno ?> - <?= $empresa->teldos ?> </p>
        </div>
    </div>

    <!-- .........BLOQUE 3 RETRIVE ITEMS.............. -->
    <!-- Mostrar los datos de los productos sin css, limpios -->
    <div class="row">
        <?php foreach ($productos as $producto): ?>
        <div class="col-md-6 col-lg-6 text-center">
            <img src="<?= base_url('uploads/productos/'.$producto['image'])?>" alt="...">
            <h4 class="font-weight-bold text-center"> <?= $producto['name'] ?></h4>
            <div class="form-group">
                <label class="h5">Descripción:</label><br />
                <?= $producto['description'] ?>
            </div>
            <div class="form-group">
                <label class="h5">Opciones:</label><br />
                <?= $producto['options'] ?>
            </div>
            <div class="form-group">
                <label class="h5">Extras:</label><br />
                <?= $producto['extras'] ?>
            </div>
            <div class="form-group">
                <label class="h5">Precio:</label>
                ¢ <?=  number_format($producto['price'], 0) ?>
            </div>

        </div>
        <?php endforeach; ?>
    </div>

</div>
<?= $this->endSection()?>