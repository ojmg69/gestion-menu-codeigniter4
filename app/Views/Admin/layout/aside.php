<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="#" class="brand-link ">
        <img src="<?= base_url('uploads/admin/logo.jpeg')?>" alt="Empresa" class="brand-image img-circle elevation-3"
            style="opacity:.8">
        <span class="brand-text font-weight-light ">
            PANEL
        </span>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column " data-widget="treeview" role="menu">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/') ?>">
                        <i class="fas fa-fw fa-home "></i>
                        <p>Inicio</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/admin/home') ?>">
                        <i class="fas fa-fw fa-columns "></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-header ">
                    AJUSTES DE LA CUENTA
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/admin/empresa') ?>">
                        <i class="fas fa-fw fa-building"></i>
                        <p>
                            Perfil Empresa
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  " href="<?= base_url('/admin/update') ?>">
                        <i class="fas fa-fw fa-lock "></i>
                        <p>
                            Cambiar Contraseña
                        </p>
                    </a>
                </li>
                <li class="nav-header ">
                    ITEMS
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/admin/producto') ?>">
                        <i class="fas fa-shopping-cart "></i>
                        <p>
                            Productos
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/admin/categoria') ?>">
                        <i class="fas fa-fw fa-tags "></i>
                        <p>
                            Categorias
                        </p>
                    </a>
                </li>
                <li class="nav-header ">
                </li>
                <li class="nav-item">
                    <a class="nav-link  "href="<?= base_url('/logout') ?>">
                        <i class="fa fa-fw fa-power-off "></i>
                        <p>
                            Salir
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>