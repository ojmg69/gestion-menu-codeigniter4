 <!-- Main Footer
 <footer class="main-footer">
    <strong>Copyright &copy; 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.

  </footer>
</div>
 ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?= base_url('public/adminlte/plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap -->
<script src="<?= base_url('public/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- AdminLTE -->
<script src="<?= base_url('public/js/croppie.min.js')?>"></script>
<script src="<?= base_url('public/adminlte/dist/js/adminlte.js')?>"></script>

<?= $this->renderSection("scripts"); ?>


</body>
</html>
