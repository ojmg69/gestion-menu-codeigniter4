<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Lista Productos
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="card p-2">

                <div class="row justify-content-center">
                    <div class="col-md-12 mb-3 mt-4"><a class="btn btn-success"
                            href="<?= base_url('/admin/producto/create') ?>"><i class="fas fa-plus"></i> Agregar</a>
                        </br>
                    </div>
                    <div class="col-md-12 m-2">

                        <h2><b>Lista de Productos</b> </h2>

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Descripción</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Categoría</th>
                                    <th scope="col">Disponible</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($productos as $producto):?>
                                <tr>
                                    <th scope="row" class="text-center"><?= $producto['name'] ?> </th>
                                    <td class="text-center"><?= $producto['description'] ?></td>
                                    <td class="text-center"><?= $producto['price'] ?></td>
                                    <?php foreach ($categorias as $categoria):?>
                                        <?php if ($categoria['id']===$producto['category_id']) : ?>
                                    <td class="text-center"><?= $categoria['name'] ?></td>
                                    <?php endif ?>
                                    <?php endforeach; ?>


                                    <?php if ($producto['active']==='1') : ?>
                                    <td class="text-center">SI</td>
                                    <?php else : ?>
                                        <td class="text-center">NO</td>
                                        <?php endif ?>
                                    <td  width=270 class="text-left">
                                    <a class="btn btn-sm btn-info" href="<?= site_url('admin/producto/show/'.$producto['id']) ?>"
                                alt="Ver producto"><i class="fas fa-eye"></i> Ver</a>
                            <a class="btn btn-sm btn-primary" href="<?= site_url('admin/producto/edit/'.$producto['id']) ?>"
                                alt="Editar producto"><i class="fas fa-edit"></i> Modificar</a><button type="button" class="btn btn-sm btn-danger"
                                            data-toggle="modal" data-target="#modal-delete<?= $producto['id'] ?>">
                                            <i class="fas fa-trash-alt"></i> Eliminar</button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="modal-delete<?=$producto['id']?>" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"><i
                                                                class="text-danger fas fa-exclamation-triangle"></i>
                                                            Notificación</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        ¿Esta Seguro que quiere eliminar?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-primary"
                                                            href="<?= site_url('admin/producto/delete/'.$producto['id']) ?>">Confirmar</a>
                                                        <button type="button" class="btn btn-danger"
                                                            data-dismiss="modal">Cancelar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                        <?php echo $paginador->links() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?= $this->endSection()?>