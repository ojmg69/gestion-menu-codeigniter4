<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Lista Productos
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="col-md-12 mb-3 mt-4"><a class="btn btn-primary" href="<?= base_url('/admin/producto') ?>"><i
                            class="fas fa-long-arrow-alt-left"></i> Regresar</a>
                    </br>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title font-weight-bold">Detalle Producto</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body box-profile">
                        <div class="row">

                            <div class="col-md-6">
                                <h4 class="font-weight-bold text-center"> <?=$producto['name'] ?></h4>
                                <div class="form-group">
                                    <label>Descripción:</label><br />
                                    <?= $producto['description'] ?>
                                </div>
                                <div class="form-group">
                                    <label>Precio:</label>
                                    <?=number_format($producto['price'], 2) ?> $
                                </div>

                                <div class="form-group">
                                    <label>Disponible:</label>
                                    <?php if ($producto['active']==='1') : ?>
                                    <td class="text-center">SI</td>
                                    <?php else : ?>
                                    <td class="text-center">NO</td>
                                    <?php endif ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <img src="<?=base_url('uploads/productos/'.$producto['image'])?>" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <?= $this->endSection()?>