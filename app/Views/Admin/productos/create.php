<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Crear Producto
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">

    <div class="row">
    
        <div class="text-danger">
            <?php echo $validation->listErrors(); ?>
        </div>
    
        <div class="col-md-12">
            <div class="col-md-10 mb-3 mt-4"><a class="btn btn-primary" href="<?= base_url('/admin/producto') ?>"><i
                        class="fas fa-long-arrow-alt-left"></i> Regresar</a>
                </br>
            </div>
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h2 class="card-title"><b>Agregar Producto</b> </h2>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form p-2" action="<?= base_url('/admin/producto/store') ?>" role="form" method="post"
                        enctype="multipart/form-data">

                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="name">Nombre:</label>
                                <input type="text" class="form-control " id="name" name="nombre" placeholder="Nombre">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="description">Descripción:</label>
                                <input type="text" class="form-control " id="description" name="descripcion"
                                    placeholder="Descripción">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="options">Opciones:</label>
                                <input type="text" class="form-control " id="options" name="opciones"
                                    placeholder="Opciones">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="extras">Extras:</label>
                                <input type="text" class="form-control " id="extras" name="extras" placeholder="extras">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="price">Precio:</label>
                                <input type="number" class="form-control " id="price" name="precio">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="category_id">Categoria:</label>
                                <select class="form-control" id="category_id" name="category_id" required>
                                <?php foreach ($categorias as $categoria):?>
                                    <option value="<?=$categoria['id']?>"><?=$categoria['name']?></option>

                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="active">Disponible:</label>
                                <select class="form-control" id="active" name="active">
                                    <option selected="selected" value="1">SI</option>
                                    <option value="0">NO</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="hidden" id="Imagecroped" name="Imagecroped">
                                <label for="imagen">Selecciona una imagen:</label>
                                <input type="file" class="item-img file" id="imagen" accept="image/jpeg" name="imagen">
                            </div>
                            <img src="" class="gambar" id="item-img-output" />
                            <span class="help-inline"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <button class='btn btn-success' type='submit' value='submit'>
                                    <i class='fas fa-pencil-alt'> </i> Agregar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card -->

            </div>


        </div>
    </div>
</div>
<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">
    
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div id="upload-demo" class="center-block"></div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" id="cropImageBtn" class="btn btn-primary">Crear</button>
                    </div>
                </div>
            </div>
        </div>
</div>

<?= $this->endSection()?>


<?=$this->section("scripts")?>
<script>
            var c = new Croppie(document.getElementById('item'));
            // call a method
            c.method(args);
        </script>

        <script>
            // Start upload preview image
            $(".gambar").attr("src", "<?= base_url('uploads/productos/default.jpg')?>");
            var $uploadCrop,
                tempFilename,
                rawImg,
                imageId;
    
            function readFile(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.upload-demo').addClass('ready');
                        $('#cropImagePop').modal('show');
                        rawImg = e.target.result;
                        
                    }
                    //reader.src = URL.createObjectURL(input.files[0]);
                    reader.readAsDataURL(input.files[0]);
                        
                } else {
                    swal("Sorry - you're browser doesn't support the FileReader API");
                }
            }
    
            $uploadCrop = $('#upload-demo').croppie({
                viewport: {
                    width: 500,
                    height: 300,
                },
                boundary: {
                    width: 500,
                    height: 300
                },
                enableExif: true
            });
            $('#cropImagePop').on('shown.bs.modal', function() {
                // alert('Shown pop');
                $uploadCrop.croppie('bind', {
                    url: rawImg
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            });
    
            $('.item-img').on('change', function() {
                //imageId = $(this)[0].data('id');
                imageId = $(this)[0].id;
                console.log(imageId);
                tempFilename = $(this).val();
                $('#cancelCropBtn').data('id', imageId);
                console.log( $('#cancelCropBtn').data('id', imageId));
                readFile(this);
            });
            $('#cropImageBtn').on('click', function(ev) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {
                        width: 500,
                        height: 300
                    }
                    
                }).then(function(resp) {
                    $('#item-img-output').attr('src', resp);
                    $('#cropImagePop').modal('hide');
    
                var img = resp;
                anchor = $("#Imagecroped");
                anchor.val(img);
                    
        
                });
            });
        </script>

<?=$this->endSection()?>
