<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Dashboard
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?= $total_productos->total ?></h3>
                    <p>Productos</p>
                </div>
                <div class="icon">
                    <i class="fas fa-shopping-cart"></i>
                </div>
                <a href="<?= base_url('/admin/producto') ?>" class="small-box-footer">
                    Ver Más <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3><?= $total_categorias->total ?></h3>

                    <p>Categorias</p>
                </div>
                <div class="icon">
                    <i class="fas fa-fw fa-tags"></i>
                </div>
                <a href="<?= base_url('/admin/categoria') ?>" class="small-box-footer">
                    Ver Más <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
    </div>
</div>

<?= $this->endSection()?>