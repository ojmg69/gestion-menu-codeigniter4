<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Detalle Empresa
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">
    <div class="row offset-md-1">
        <div class="col-md-6">
            <!-- About Me Box -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title font-weight-bold">Perfil Empresa</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="img-thumbnail" style="max-width: 250px;
                 height: auto;" src="<?= base_url('uploads/admin/'.$empresa->image)?>" alt="Empresa">
                    </div>
                    <div class="col text-center">
                        <h3 class="font-weight-bold"><?= $empresa->name ?></h3>
                    </div>
                    <br />
                    <strong><i class="fas fa-book mr-1"></i> Descripción</strong>

                    <p class="text-muted">
                        <?= $empresa->about ?>
                    </p>
                    <hr>
                    <strong><i class="fas fa-map-marker-alt mr-1"></i> Dirección</strong>
                    <p class="text-muted"><?= $empresa->address ?></p>
                    <hr>
                    <strong><i class="fas fa-map-marker-alt mr-1"></i> Provincia</strong>
                    <p class="text-muted"> <?= $empresa->provincia ?></p>
                    <hr>
                    <strong><i class="fas fa-map-marker-alt mr-1"></i> Cantón</strong>
                    <p class="text-muted"><?= $empresa->canton ?></p>
                    <hr>
                    <strong><i class="fas fa-phone-alt mr-1"></i> Telefonos</strong>

                    <p class="text-muted"> <?= $empresa->teluno ?> - <?= $empresa->teldos ?> </p>
                    <hr>
                    <strong><i class="fas fa-envelope"></i> Email Administrador</strong>

                    <p class="text-muted"> <?= $user->email ?> </p>
                </div>
                <div class="col pb-2">
                    <a class="btn btn-primary" href="<?= site_url('admin/empresa/edit/'.$empresa->id) ?>"
                        alt="Modificar Empresa"><i class="fas fa-edit"></i> Modificar</a>
                </div>

            </div>
            <!-- /.card -->
        </div>

    </div>


    
</div>



<?= $this->endSection()?>