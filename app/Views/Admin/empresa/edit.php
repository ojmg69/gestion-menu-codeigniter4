<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Editar Empresa
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">
<div class="row justify-content-center">
        <div class="col-md-12 mb-1 mt-1"><a class="btn btn-primary" href="<?= base_url('/admin/empresa') ?>"><i
                    class="fas fa-long-arrow-alt-left"></i> Regresar</a>
            </br>
        </div>
        <div class="col-md-12 mb-2 mt-1">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h2 class="card-title"><b>Editar Perfil</b> </h2>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form p-3" action="<?= base_url('/admin/empresa/update') ?>" role="form" method="post"
                        enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" value="<?=$empresa['id']?>" name="id">
                            <div class="col-md-4 form-group">
                                <label for="name">Nombre:</label>
                                <input type="text" class="form-control " id="name" name="nombre"  value="<?=$empresa['name']?>">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="about">Descripción:</label>
                                <input type="text" class="form-control " id="about" name="descripcion"  value="<?=$empresa['about']?>">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="address">Dirección:</label>
                                <input type="text" class="form-control " id="address" name="direccion"  value="<?=$empresa['address']?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 form-group">
                                <label for="provincia">Provincia:</label>
                                <input type="text" class="form-control " id="provincia" name="provincia"  value="<?=$empresa['provincia']?>">
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="canton">Cantón:</label>
                                <input type="text" class="form-control " id="canton" name="canton"  value="<?=$empresa['canton']?>">
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="teluno">Telefono Uno:</label>
                                <input type="text" class="form-control " id="teluno" name="teluno"  value="<?=$empresa['teluno']?>">
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="teldos">Telefono Dos:</label>
                                <input type="text" class="form-control " id="teldos" name="teldos"  value="<?=$empresa['teldos']?>">
                            </div>
                        </div>

                       <div class="row">
                            <div class="col-md-3 form-group">
                            <input type="hidden" id="Imagecroped" name="Imagecroped">
                            <label for="imagen">Selecciona una imagen:</label>
                            <input type="file" class="item-img file" id="imagen"  accept="image/jpeg" name="imagen"> 
                        </div>
                        <img src="" class="gambar" id="item-img-output" />
                        <span class="help-inline"></span>
                </div>

                   <button class='btn btn-success' type='submit' value='submit'>
                    <i class='fas fa-pencil-alt'> </i> Actualizar
                </button>
                </form>
            </div>
            <!-- /.card -->

        </div>
    </div>
    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">
    
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div id="upload-demo" class="center-block"></div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" id="cropImageBtn" class="btn btn-primary">Crear</button>
                    </div>
                </div>
            </div>
        </div>
</div>
<?= $this->endSection()?>

<?=$this->section("scripts")?>
<script>
            var c = new Croppie(document.getElementById('item'));
            // call a method
            c.method(args);
        </script>

        <script>
            // Start upload preview image
            $(".gambar").attr("src", "<?= base_url('uploads/admin/'.$empresa['image'])?>");
            var $uploadCrop,
                tempFilename,
                rawImg,
                imageId;
    
            function readFile(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.upload-demo').addClass('ready');
                        $('#cropImagePop').modal('show');
                        rawImg = e.target.result;
                        
                    }
                    //reader.src = URL.createObjectURL(input.files[0]);
                    reader.readAsDataURL(input.files[0]);
                        
                } else {
                    swal("Sorry - you're browser doesn't support the FileReader API");
                }
            }
    
            $uploadCrop = $('#upload-demo').croppie({
                viewport: {
                    width: 200,
                    height: 200,
                },
                boundary: {
                    width: 200,
                    height: 200
                },
                enableExif: true
            });
            $('#cropImagePop').on('shown.bs.modal', function() {
                // alert('Shown pop');
                $uploadCrop.croppie('bind', {
                    url: rawImg
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            });
    
            $('.item-img').on('change', function() {
                //imageId = $(this)[0].data('id');
                imageId = $(this)[0].id;
                console.log(imageId);
                tempFilename = $(this).val();
                $('#cancelCropBtn').data('id', imageId);
                console.log( $('#cancelCropBtn').data('id', imageId));
                readFile(this);
            });
            $('#cropImageBtn').on('click', function(ev) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {
                        width: 200,
                        height: 200
                    }
                    
                }).then(function(resp) {
                    $('#item-img-output').attr('src', resp);
                    $('#cropImagePop').modal('hide');
    
                var img = resp;
                anchor = $("#Imagecroped");
                anchor.val(img);
                    
        
                });
            });
        </script>

<?=$this->endSection()?>
