<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Editar
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">
    <div class="row">
        <div class="col-md-8 mb-3 mt-4"><a class="btn btn-primary" href="<?= base_url('/admin/empresa') ?>"><i
                    class="fas fa-long-arrow-alt-left"></i> Regresar</a>
            </br>
        </div>
        <div class="col-md-8 mb-2 mt-2">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h2 class="card-title"><b>Cambiar Contraseña</b> </h2>
                </div>
                <form class="form p-3" action="<?= base_url('/admin/update') ?>" role="form" method="post"
                    enctype="multipart/form-data" autocomplete="off">

                    <div class="row">
                        <input type="hidden" value="<?=$user->id?>" name="id">
                        <div class="col-md-12 form-group">

                            <label for="email">Email:</label>
                            <input type="email" class="form-control " id="email" name="email" value="<?= $user->email?>">
                        </div>
                        
                        <div class="col-md-12 form-group">
                            <label for="password">Contraseña:</label>
                            <input type="text" class="form-control " id="password" name="password" autocomplete="off"
                                value="">
                                <b class="text-danger">Importante: en caso de no cambiar la contraseña dejar en blanco</b>
                        </div>

                    </div>
            </div>


            <button class='btn btn-success' type='submit' value='submit'>
                <i class='fas fa-pencil-alt'> </i> Actualizar
            </button>
            </form>
        </div>
        <!-- /.card -->

    </div>
</div>

</div>



<?= $this->endSection()?>