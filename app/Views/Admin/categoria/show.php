<?= $this->extend('Front/layout/main')?>
<?= $this->section('content')?>

<div class="container">
    <h2><?= $categoria['name'] ?></h2>
<div class="row">
        <?php foreach ($productos as $producto): ?>
        <div class="col-md-6 col-lg-6 text-center">
            <img src="<?= base_url('uploads/productos/'.$producto['image'])?>" alt="...">
            <h4 class="font-weight-bold text-center"> <?= $producto['name'] ?></h4>
            <div class="form-group">
                <label class="h5">Descripción:</label><br />
                <?= $producto['description'] ?>
            </div>
            <div class="form-group">
                <label class="h5">Opciones:</label><br />
                <?= $producto['options'] ?>
            </div>
            <div class="form-group">
                <label class="h5">Extras:</label><br />
                <?= $producto['extras'] ?>
            </div>
            <div class="form-group">
                <label class="h5">Precio:</label>
                ¢ <?=  number_format($producto['price'], 0) ?>
            </div>

        </div>
        <?php endforeach; ?>
    </div>

</div>
<?= $this->endSection()?>