<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Lista Categorias
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">
    <div class="row">
        <div class="col-md-10">

            <div class="card">

                <div class="row justify-content-center">
                    <div class="col-md-10 mb-3 mt-4"><a class="btn btn-success"
                            href="<?= base_url('/admin/categoria/create') ?>"><i class="fas fa-plus"></i> Agregar</a>
                        </br>
                    </div>
                    <div class="col-md-10 m-2">

                        <h2><b>Lista de Categorías</b> </h2>

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col">ID</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php if(is_array($categorias) && count($categorias)>0):
                                 foreach ($categorias as $categoria):?>
                                <tr>
                                    <th scope="row" class="text-center"><?= $categoria['id'] ?> </th>
                                    <td class="text-center"><?= $categoria['name'] ?></td>
                                    <td class="text-center">No Asociada</td>
                                    <td class="text-left"><button type="button" class="btn btn-sm btn-danger"
                                            data-toggle="modal" data-target="#modal-delete<?= $categoria['id'] ?>">
                                            <i class="fas fa-trash-alt"></i> Eliminar</button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="modal-delete<?=$categoria['id']?>" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"><i
                                                                class="text-danger fas fa-exclamation-triangle"></i>
                                                            Notificación</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        ¿Esta Seguro que quiere eliminar?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-primary"
                                                            href="<?= site_url('admin/categoria/delete/'.$categoria['id']) ?>">Confirmar</a>
                                                        <button type="button" class="btn btn-danger"
                                                            data-dismiss="modal">Cancelar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach;
                                endif; ?>

                                <?php if(is_array($con_categorias) && count($con_categorias)>0):
                                foreach ($con_categorias as $con_categoria):?>
                                <tr>
                                    <th scope="row" class="text-center"><?= $con_categoria['id'] ?> </th>
                                    <td class="text-center"><?= $con_categoria['name'] ?></td>
                                    <td class="text-center"><span class="glyphicon glyphicon-pushpin">Asociada</span>
                                    </td>
                                    <td class="text-left">
                                        <a class="btn btn-sm btn-primary"
                                            href="<?= site_url('admin/categoria/edit/'.$con_categoria['id']) ?>"><i
                                                class="fas fa-pencil-alt"> </i> Modificar</a>
                                    </td>
                                </tr>
                                <?php endforeach;
                                 endif; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<!--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="text-danger fas fa-exclamation-triangle"></i>
                    Notificación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta Seguro que quiere eliminar?
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary"
                    href="#">Confirmar</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>-->


<?= $this->endSection()?>