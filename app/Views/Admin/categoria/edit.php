<?= $this->extend('Admin/layout/main')?>
<?= $this->section('title')?>
Lista Categorias
<?= $this->endSection()?>

<?= $this->section('content')?>

<div class="container">

    <div class="row">
    <div class="text-danger">
      <?php echo $validation->listErrors(); ?>
    </div>
        <div class="col-md-10">
            <div class="col-md-10 mb-3 mt-4"><a class="btn btn-primary" href="<?= base_url('/admin/categoria') ?>"><i
                        class="fas fa-long-arrow-alt-left"></i> Regresar</a>
                </br>
            </div>
            <div class="col-md-10">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h2 class="card-title"><b>Editar Categoría</b> </h2>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form p-3" action="<?= base_url('/admin/categoria/update') ?>" role="form" method="post"
                        enctype="multipart/form-data">

                        <div class="row">
                        <input type="hidden" value="<?=$categoria['id']?>" name="id">
                            <div class="col-md-12 form-group">
                                <label for="name">Nombre:</label>
                                <input type="text" class="form-control " id="name" name="nombre" placeholder="Nombre" value="<?=$categoria['name']?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">

                                <button class='btn btn-success' type='submit' value='submit'>
                                    <i class='fas fa-pencil-alt'> </i> Agregar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card -->

            </div>


        </div>
    </div>
</div>



<?= $this->endSection()?>